# TO RUN THIS
# bash <(curl https://czuniga.bitbucket.io/init.sh)

echo "Go get your ssh keys... I'll wait..."
read -rs -k 1 answer

ssh-add

# Install Homeshick
echo "Installing Homeshick"
git clone https://github.com/andsens/homeshick.git $HOME/.homesick/repos/homeshick

source .homesick/repos/homeshick/homeshick.sh

# Install my castle
echo "Installing dotfiles castle"
homeshick clone git@bitbucket.org:czuniga/dotfiles.git
homeshick link

zsh $HOME/.homesick/repos/dotfiles/init.zsh